﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aspDay6.Models
{
    public class catalog
    {
        public int id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }

        public catalog()
        {
            myCatNews = new List<news>();
        }
        public virtual List<news> myCatNews { get; set; }
    }
}