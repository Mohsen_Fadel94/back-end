﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace aspDay6.Models
{
    public class user
    {
        public int id  { get; set; }
        public string name  { get; set; }
        [Required(ErrorMessage = "*")]

        public string password  { get; set; }
        [Required(ErrorMessage ="*")]
        public string email  { get; set; }
        public int age  { get; set; }
        public string address { get; set; }
        public string img { get; set; }

        public user()
        {
            myNews = new List<news>();
        }
       public virtual List<news> myNews { get; set; }
    }
}