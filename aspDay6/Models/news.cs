﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace aspDay6.Models
{
    public class news
    {
        public int id  { get; set; }
        public string title  { get; set; }
        public string bref  { get; set; }
        public string details  { get; set; }
     
        public string image  { get; set; }
        public DateTime date  { get; set; }
        public string attach  { get; set; }

        [ForeignKey("myUser")]
        public int? userId { get; set; }
        [ForeignKey("myCatalog")]
        public int? catId { get; set; }

        public virtual user myUser { get; set; }
        public virtual catalog myCatalog { get; set; }
    }
}