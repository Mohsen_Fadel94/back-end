namespace aspDay6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class verg1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.catalogs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        desc = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.news",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(),
                        bref = c.String(),
                        details = c.String(),
                        image = c.String(),
                        date = c.DateTime(nullable: false),
                        attach = c.String(),
                        userId = c.Int(),
                        catId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.catalogs", t => t.catId)
                .ForeignKey("dbo.users", t => t.userId)
                .Index(t => t.userId)
                .Index(t => t.catId);
            
            CreateTable(
                "dbo.users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        password = c.String(nullable: false),
                        email = c.String(nullable: false),
                        age = c.Int(nullable: false),
                        address = c.String(),
                        img = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.news", "userId", "dbo.users");
            DropForeignKey("dbo.news", "catId", "dbo.catalogs");
            DropIndex("dbo.news", new[] { "catId" });
            DropIndex("dbo.news", new[] { "userId" });
            DropTable("dbo.users");
            DropTable("dbo.news");
            DropTable("dbo.catalogs");
        }
    }
}
