﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using aspDay6.Models;

namespace aspDay6.Controllers
{
    public class newsController : Controller
    {
        route db = new route();

        // GET: news
        public ActionResult create()
        {
            if(Session["userId"] != null)
            {
                ViewBag.cat = new SelectList(db.Catalogs.ToList(), "id", "name");

                return View();
            }
            else
            {
                return RedirectToAction("login", "user");
            }
        }
        [HttpPost]
        public ActionResult create(news n, HttpPostedFileBase photo)
        {
            photo.SaveAs(Server.MapPath($"~/attach/{photo.FileName}"));
            n.image = photo.FileName;
            n.date = DateTime.Now;
            n.userId = int.Parse(Session["userId"].ToString());
            db.News.Add(n);
            db.SaveChanges();
            return RedirectToAction("userNews");
        }
        public ActionResult userNews()
        {
            if(Session["userId"] != null)
            {
                int id = int.Parse(Session["userId"].ToString());
                List<news> ne = db.News.Where(n => n.userId == id).ToList();
            return View(ne);
            }
            else
            {
                return RedirectToAction("login");
            }
        }
        public ActionResult categoryNews(int catid)
        {
      
            List<news> s = db.News.Where(n => n.catId == catid).ToList();
            return PartialView("userNews", s);


        }



        public  ActionResult details(int id)
        {
            news d = db.News.Where(n => n.id == id).FirstOrDefault();
            return View(d);
        }
        public ActionResult allnews()
        {
            List<news> ns = db.News.ToList();
            return View("userNews", ns);
        }
        public ActionResult newsbycatalog()
        {
            ViewBag.cat = new SelectList(db.Catalogs.ToList(), "id", "name");
            return View();
        }
        public ActionResult newsPartial(int catid)
        {
            List<news> s = db.News.Where(n => n.catId == catid).ToList();
            return PartialView(s);
        }
        public ActionResult search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult searchnews(string txt)
        {
            List<news> sn = db.News.Where(m => m.title.Contains(txt) || m.bref.Contains(txt)|| m.details.Contains(txt)).ToList();

            return PartialView("newsPartial", sn);
                }
    }
    
}