﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using aspDay6.Models;

namespace aspDay6.Controllers
{
    public class userController : Controller
    {
        route db = new route();
        // GET: user
        public ActionResult login()
        {
            if(Request.Cookies["route"] != null)
            {
                Session.Add("userId",  Request.Cookies["route"].Values["id"]);
                return RedirectToAction("profile");
            }
            return View();
        }

        [HttpPost]
        public ActionResult login(user u , string check)
        {
            user s = db.Users.Where(n => n.email == u.email && n.password == u.password).FirstOrDefault();
            if(s!= null)
            {
                if(check == "true")
                {
                    HttpCookie co = new HttpCookie("route");
                    co.Values.Add("id", s.id.ToString());
                    co.Expires = DateTime.Now.AddDays(30);
                    Response.Cookies.Add(co);
                }
                //login
                Session.Add("userId", s.id);
                return RedirectToAction("profile");
            }
            else
            {
                ViewBag.status = "invalid your name or passord";
               //not login
            return View();
            }
        }
        public ActionResult profile()
        {
          if(Session["userId"] != null)
            {
                int id = int.Parse(Session["userId"].ToString());
                user s = db.Users.Where(n => n.id == id).FirstOrDefault();
                return View(s);
            }
            else
            {
                return RedirectToAction("login");
            }
        }
        public ActionResult logout()
        {
            Session["userId"] = null;
            HttpCookie co = new HttpCookie("route");
            co.Expires = DateTime.Now.AddDays(-10);
            Response.Cookies.Add(co);
            return RedirectToAction("login");
        }


        public ActionResult register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult register(user r , HttpPostedFileBase image)
        {
            user s = db.Users.Where(n => n.email == r.email).FirstOrDefault();
            if (s != null)
            {
                ViewBag.status = "This email already here";
                //not login
                return View();

             
            }
            else
            {
                
                //file
                string path = $"~/attach/{image.FileName}";
                image.SaveAs(Server.MapPath(path));

                r.img = image.FileName;
                db.Users.Add(r);
                db.SaveChanges();
                return RedirectToAction("login");
            }
        
            
        }
    }








}